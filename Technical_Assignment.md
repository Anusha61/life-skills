# Service-oriented architecture

SOA, or service-oriented architecture, defines a way to ensure the reuse of software components through service interfaces. These interfaces use common communication standards in such a way that they can be quickly incorporated into new applications without the need for deep integration each time.

Each service in an SOA embodies the integration of code and data necessary to perform a complete, discrete business function (eg, checking a customer's credit, calculating a monthly loan payment, or processing a mortgage application). Service interfaces provide loose coupling, meaning they can be called with little or no knowledge of how the integration is implemented underneath. Services are exposed using standard network protocols—such as SOAP (Simple Object Access Protocol)/HTTP or JSON/HTTP—to send requests to read or change data. Services are published in a way that allows developers to quickly find and reuse them to build new applications.

These services can be created from scratch, but are often created by exposing functionality from legacy systems of record as service interfaces.

In this way, SOA represents an important stage in the evolution of application development and integration over the past few decades. Before SOA emerged in the late 1990s, connecting an application to data or functionality stored in another system required complex point-to-point integration—integration that developers had to partially or completely recreate for each new development project. Making these features available through SOA eliminates the need to recreate deep integration each time


## Five benefits of using Service-Oriented Architecture:

### Effective and easy expansion of business processes
SOA allows SynQ to provide a set of modular software components (called services), each of which corresponds to a common warehouse process (such as warehouse, picking or inventory management). These services are organized to meet the needs of the warehouse. As these are commonly recognized services, when an external system is integrated with SynQ, information about these processes is shared and those already in place are marked as "implemented". This shared knowledge can significantly speed up system integration and can reduce implementation time for both Swisslog and the ERP developer.

After this integration is complete, if something is missing, the modular nature of SynQ SOA allows Swisslog to extend the common interface and introduce a new component that is tailored to the needs of a specific application.

### Unique and universally recognized communication architecture
SOA is an international standard recognized by major technical communities worldwide. Most ERPs are now capable of integration and integration into SOA. This means that the standard SynQ interface can communicate with surrounding systems when first installed, bridging the technical gap between the two systems.

In addition, these days, both systems are not required to use the same programming language or even share the same middleware components (such as file systems or databases) to exchange data, as the data transfer takes place over a network, reducing latency. from outdated add-on components.

### High speed in the circulation of information between systems
SOA, combined with event-driven patterns (we'll cover this in more detail in the next blog post - but basically it's an architecture that uses the production, detection, consumption and response to events to create patterns when actions are performed), increases the speed of information exchange between by various involved systems.

This combination exponentially increases the speed of information between SynQ and the host system, and information can be retrieved in real time. This real-time data can be used to provide inventory and/or supply updates so that managers can plan and regulate incoming and outgoing goods based on the actual state of the warehouse rather than hypothetical projections.
 
### Reducing the cost of software management and upgrades
The modularity of SOA helps software managers reduce costs in the software life cycle. Any updates or modifications can be implemented without affecting the proper functioning of other services, which means that the warehouse significantly reduces downtime when updating, modifying or servicing software. If an organization wishes to upgrade or modify its warehouse, it is possible to add new services that extend or change the functionality of the system without affecting any of the existing services.

### Real-time inventory update
As we touched on in point 3, the adoption of SOA allows SynQ to update the company's ERP system in real time according to the actual state of the warehouse. This situation is the result of a combination of SOA technology and the SynQ interface provided to the host system. By integrating the host system using a native SynQ standard services interface, both systems can benefit from the SynQ warehouse framework.

This framework takes advantage of Swisslog's extensive global logistics experience and includes an up-to-date view of warehouse status, enabling managers to respond to business needs in real-time and anticipate potential critical issues such as missing inventory or managing customer orders. It also enables simple quality control of goods and management of expiry of expired goods. SynQ's dynamic and fast communication enables the ERP system to obtain information without the latency experienced by systems prior to SynQ's Industry 4.0 solution.

## Implementation

SOA is independent of vendors and technologies. This means a wide variety of products can be used to implement the architecture. The decision of what to use depends on the end goal of the system.

SOA is typically implemented with web services such as simple object access protocol (SOAP) and web services description language (WSDL). Other available implementation options include:

    Windows Communication Foundation (WCF);
    gRPC; and
    messaging -- such as with Java Message Service (JMS), ActiveMQ and RabbitMQ.

SOA implementations can use one or more protocols and may also use a file system tool to communicate data. The protocols are independent of the underlying platform and programming language. The key to a successful implementation is the use of independent services that perform tasks in a standardized way, without needing information about the calling application, and without the calling application requiring knowledge of the tasks the service performs.

Common applications of SOA include:

    SOA is used by numerous armies and air force to deploy situational awareness systems.
    Healthcare organizations use SOA to improve healthcare delivery.
    SOA allows Mobile apps and games to use the mobile device's built-in functions, such as GPS.
    Museums use SOA to create virtualized storage systems for their information and content.

## WS and WSDL models of SOA

Initially, SOA implementations were based on RPC and object-broker technologies available around 2000. Since then, two SOA models have been formed:

    The web services (WS) model, which represents highly architected and formalized management of remote procedures and components.
    The representational state transfer (REST) model, which represents the use of internet technology to access remotely hosted components of applications.

The WS model of SOA uses WSDL to connect interfaces with services and SOAP to define procedure or component APIs. WS principles link applications via an enterprise service bus (ESB), which helps businesses integrate their applications, ensure efficiency and improve data governance. This approach never gained broad traction and is no longer favored by developers.

The REST model has instead gained attention. RESTful APIs offer low overhead and are easy to understand.

## Understanding

Web services promote an associate degree atmosphere for systems that are loosely coupled and practical. Many of the ideas for net services come back from an abstract design referred to as service-oriented design (SOA). SOA configures entities (services, registries, contracts, and proxies) to maximize loose coupling and recycle. This material describes these entities abstractly associate degree in their configuration.

Although you may most likely use net services to implement your service-oriented design, this chapter explains SOA without much reference to selected implementation technology. In later chapters, this is done so that you’ll see the areas within which net services deliver some good aspects of a real SOA and alternative areas within which Web services fall short. Although the net services area unit is an honest beginning toward service-oriented design, this chapter can discuss what a totally enforced SOA entails.
What can you do with SOA?

There are several things that can be done with SOA:

1. Making a Reliable Service 

It could be used to make a reliable service.

It could be used to make a reliable service which contains the following features:

    Improved information flow.
    Ability to expose internal functionality.
    Organizational flexibility.

2. Making Reusable Service

One of the main use of SOA is to make a reusable service. Therefore, SOA concepts could be easily used and implemented to make a service that is not limited to a single component but could be used in multiple components.

3. Configuration Flexibility

It is highly flexible and could be easily configured as per our needs.

4. For Developing new Function Combinations

It could be used for developing new functions combinations rapidly as per need or requirement.


## Advantages:

- Maintenance is Easy: Editing and updating any service implemented under SOA architecture is easy. You don’t need to update your system. A third party maintains the service, and any amendment in this service won’t have an effect on your system. In most cases, the previous API work because it is functioning before.
- Quality of Code Improved: As services run freelance of our system, they have their own variety of code; therefore, our code is prevented from redundancy. Also, our code becomes error-free.
- Platform Independence: Services communicate with alternative applications through a common language, which implies it’s freelance of the platform on which that application is running. Services can provide API in different languages, e.g. PHP, JavaScript, etc.
- Scalable: If any service obtains several users, it is often simply scalable by attaching additional servers. This will create service out there all time to the users.
- Reliable: Services square measure typically tiny size as compared to the full-fledged application. So it’s easier to correct and check the freelance services.
- Same Directory Structure: Services have an equivalent directory structure so customers can access the service information from an equivalent directory on every occasion. If any service has modified its location, then the additional directory remains the same. This is very helpful for consumers.
- Independent of Other Services: Services generated using SOA principles are independent of each other. So services are often utilized by multiple applications at an equivalent time.

## Disadvantages:

- High Bandwidth Server: Therefore, net service sends and receives messages and knowledge often times, so it simply reaches high requests per day. So it involves a high-speed server with plenty of information measures to run an internet service.
- Extra Overload: In SOA, all inputs square measure its validity before it’s sent to the service. If you are victimization multiple services, then it’ll overload your system with further computation.
- High Cost: It is expensive in terms of human resources, development, and technology.

## Why Should we Use SOA?

This has multiple advantages, as we have discussed earlier in this article. We can use it for making reliable, better, injectable and reusable services.
Why do we Need SOA?

It could be used for solving various business needs:

For Developing Independent Services: It is required if our business need is to develop multiple services which are independent of each other. However, these services can still communicate with each other.
To Expose Data: Exposing the functionality of the software as a service is easier to implement if we are using SOA.
To Develop Reusable Service: If our requirement is to develop a reusable service, then SOA is perfect for this. It could be used to make independent, reliable and reusable services.


## How will this Technology Help in your Career Growth?

This is one of the most used technology. Almost every business logic have requirements that require some or the other way input from SOA. Hence it is not wrong to say that SOA will be demanded a large number of different business requirements.

## Conclusion

It is a multi-purpose concept for designing different injectable services. These services could act externally to other components as application components through a communication protocol over a network. The basic principle of SOA does not depend upon technologies, products, and vendors. However, it requires a good amount of practice so that it could be used in the most efficient requirements.


## Reference

[https://www.educba.com/what-is-soa/](https://www.educba.com/what-is-soa/)
