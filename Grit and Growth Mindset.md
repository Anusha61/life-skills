# Grit and Growth Mindset

### 1. Paraphrase (summarize) the video in a few lines. Use your own words.

 The video talks about grit and how the growth mindset can help with grit. Whatever we have as a success or failure, we should always test them to grow our minds.

### 2. What are your key takeaways from the video to take action on?

- Never be afraid to test yourself
- Never take a failure as a permanent decision
- Always try to grow your mind by checking your ideas

### 3. Paraphrase (summarize) the video in a few lines in your own words.

The video talks about the different mindsets which it considers two types of mindset

- Fixed Mindset
- Growth Mindset

Fixed mindset people believe that skill and intelligence are set whether you have them or not. But, growth mindset people believe that they can learn any skills and practice them to master them.
Fixed mindset people believe that skills are born and growth mindset people believe skills can be learned.

### 4. What are your key takeaways from the video to take action on?
- Video talks about the importance of growth mindset and Fixed mindset people.
- Fixed mindset people believe that skills and intelligence are something you are born with and cannot be improved.
- Growth mindset people believe that skills are something that you can get good at by practicing and becoming the master of it.
- People with a growth mindset focus on learning and if they face failure they do not take it as a destiny for them.
 People with a growth mindset welcome constructive criticism and use that to learn from their mistakes.


### 5, What is the Internal Locus of Control? What is the key point in the video?

Locus of Control refers to an individual's perception of the underlying main causes of events in his/her life. Or, more simply:
you believe that your destiny is controlled by yourself or by external forces

### 6. Paraphrase (summarize) the video in a few lines in your own words.

The video talks about how we can develop our grit and growth mind. The first to believe in your abilities. The fundamental rule is to believe in your abilities and always try to learn new skills with a growth mindset. to believe create a mental structure.

### 7. What are your key takeaways from the video to take action on?

Some of the takeaways from the video are listed:-
- Believe in your abilities to figure out whatever things you are stuck with.
- Always question your mind and the thoughts are they negative or positive?
- If you are struggling with something do not get discouraged and try to reach the result.

### 8. What are one or more points that you want to take action on from the manual? (Maximum 3)

- I will use the weapons of Documentation, Google, Stack Overflow, GitHub Issues, and the Internet before asking for help from fellow warriors or looking at the solution.
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.
- I am very good friends with Confusion, Discomfort, and Errors. Confusion tells me there is more understanding to be achieved Discomfort tells me I have to make an effort to understand. I understand the learning process is uncomfortable. Errors tell me what is not working in my code and how to fix it.
